

#include "ListviewUi.h"

void MyListViewMakeColumn(HWND hLstView)
{
	LVCOLUMN ListCol;

	ListView_SetExtendedListViewStyle(hLstView, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	ListCol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	ListCol.fmt = LVCFMT_LEFT;

	ListCol.cx = PickMyData(13);
	ListCol.pszText = PickMyWideStringFromIndex(17); //Index of column
	ListCol.iSubItem = 0;
	ListView_InsertColumn(hLstView, 0, &ListCol);

	ListCol.cx = PickMyData(14);
	ListCol.pszText = PickMyWideStringFromIndex(18);	//message
	ListCol.iSubItem = 1;
	ListView_InsertColumn(hLstView, 1, &ListCol);

	ListCol.cx = PickMyData(15);
	ListCol.pszText = PickMyWideStringFromIndex(19); //time
	ListCol.iSubItem = 2;
	ListView_InsertColumn(hLstView, 2, &ListCol);

	ListCol.cx = PickMyData(16);
	ListCol.pszText = PickMyWideStringFromIndex(100); //unknown
	ListCol.iSubItem = 3;
	ListView_InsertColumn(hLstView, 3, &ListCol);
}

void ConsoleLogListview(MyMainProc *mProc, HWND hWnd)
{
	UI_SET *appUi = mProc->ui_ptr;
	HWND hList = CreateWindow(WC_LISTVIEW, NULL, WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT | LVS_SINGLESEL | LVS_OWNERDATA,
		30, 30, PickMyData(22), PickMyData(25), hWnd, (HMENU)CON_LOG_LIST, g_hInst, NULL);

	SendMessage(hList, WM_SETFONT, (WPARAM)appUi->font1, 1);
	MyListViewMakeColumn(hList);
	appUi->conList = hList;
}