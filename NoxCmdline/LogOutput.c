
#include "LogOutput.h"
#include "noxMem.h"
#include "nox_func.h"
#include "general.h"


void LogFileName(char **dest)
{
	int dSize = 100, *ptr = (int *)dest;
	char *sTmp = (char *)malloc(sizeof(char) * dSize);
	SYSTEMTIME sTime;

	GetLocalTime(&sTime);
	sprintf_s(sTmp, (size_t)dSize, PickMyStringFromIndex(88), sTime.wYear, sTime.wMonth, sTime.wDay, sTime.wHour, sTime.wMinute, sTime.wSecond);
	*ptr = (int)sTmp;
}

char* WriteWideToMulti(wchar_t *src, int *destLen)
{
	int dSize = lstrlen(src) + 1, cur = 0;
	char *sDest = (char *)malloc(sizeof(char) * dSize);

	for (int i = 0; i < dSize; i++)
	{
		if (src[i] < 0x80)
			sDest[cur++] = (char)src[i];
		else
		{
			sDest[cur++] = (char)(src[i] & 0xff);
			sDest[cur++] = (char)(src[i] >> 8);
		}
	}
	*destLen = cur;
	return sDest;
}

int MyWideCharToMultiByte(wchar_t *src, char **dest)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, src, -1, NULL, 0, NULL, NULL);

	WideCharToMultiByte(CP_ACP, 0, src, -1, *dest, nLen, NULL, NULL);
	return nLen;
}

void WriteLogFile(FILE *fp, ConsoleMsg *cMsg)
{
	int nTime = cMsg->time, destLen = 0;
	char sTime[40] = { 0, }; // , *log = NULL;
	char logData[516] = { 0, };
	char *dPtr = logData;

	sprintf_s(sTime, sizeof(sTime), PickMyStringFromIndex(89), (nTime >> 16) & 0xff, (nTime >> 8) & 0xff, nTime & 0xff);
	fwrite(sTime, sizeof(char), strlen(sTime), fp);
	destLen = MyWideCharToMultiByte((wchar_t *)cMsg->content, &dPtr);
	logData[strlen(logData)] = '\n';
	fwrite(logData, strlen(logData), sizeof(char), fp);
}

int OutputConsoleLog(ConsoleDat *conData, int logCount)
{
	int res = 0;
	FILE *fp = NULL;
	char *fName = NULL;

	LogFileName(&fName);
	if (fName != NULL)
	{
		fopen_s(&fp, fName, "wb");
		if (fp != NULL)
		{
			for (int i = 0; i < logCount; i++)
				WriteLogFile(fp, conData->cMsg[i]);
			fclose(fp);
			res = 1;
		}
		else
			res = 2;
		free(fName);
	}
	return res;
}

int WriteAutoLogFile(char *fName, int fStart, int fEnd, ConsoleMsg **cMsg, int logMax)
{
	FILE *fp = NULL;
	int fSize;

	fopen_s(&fp, fName, "at");
	if (fp != NULL)
	{
		fseek(fp, 0, 2);
		fSize = ftell(fp);
		if (fSize > logMax)
		{
			fwrite(PickMyStringFromIndex(106), sizeof(char), PickMyStringLenFromIndex(106), fp);
			fclose(fp);
			remove(PickMyStringFromIndex(103));
			rename(fName, PickMyStringFromIndex(103));
			return 0;
		}
		else
		{
			for (int i = fStart; i < fEnd; i++)
				WriteLogFile(fp, cMsg[i]);
			fclose(fp);
		}
	}
	return 1;
}

int FindTempLogW(char *src, int srcLen)
{
	int pic = -1;

	for (int i = 0; i < srcLen; i++)
	{
		if (src[i] == '\\')
			pic = i;
	}
	return pic;
}

int GetTempLogPath(char *src, char **dest)
{
	char *path = NULL;
	int srcLen = strlen(src);
	int endSymbPos = FindTempLogW(src, srcLen);

	if (endSymbPos > 0)
	{
		MyBufferRealloc(&path, endSymbPos + 1);
		for (int i = 0; i <= endSymbPos; i++)
			path[i] = src[i];
		*dest = path;
	}
	return endSymbPos;
}

void RenameTempLogFile(char *tempFileName, void *sTimePtr)
{
	SYSTEMTIME *sysTime = sTimePtr;
	char *rName = NULL, *logPath = NULL;
	int rSize = 50;

	rSize += GetTempLogPath(tempFileName, &logPath);
	if (logPath == NULL) return;
	MyBufferRealloc(&rName, rSize);
	sprintf_s(rName, rSize, PickMyStringFromIndex(150),
		logPath, sysTime->wYear, sysTime->wMonth, sysTime->wDay, sysTime->wHour, sysTime->wMinute, sysTime->wSecond);
	rename(tempFileName, rName);
	free(rName);
	free(logPath);
}

void NotifyChangeTempLogFile(MyMainProc *mProc)
{
	char notifyMsg[200] = { 0, };
	SYSTEMTIME sysTime;

	GetLocalTime(&sysTime);
	sprintf_s(notifyMsg, sizeof(notifyMsg), PickMyStringFromIndex(146), 
		sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);

	RenameTempLogFile(PickMyStringFromIndex(103), &sysTime);
	NoxConsolePrint(mProc, notifyMsg, 13);
	SetWindowText(mProc->ui_ptr->hEditOut, PickMyWideStringFromIndex(147));
}

void AutoLogOutput(ConsoleDat *conData)
{
	int *cLine = &conData->conMsgCount;
	int *wLine = &conData->fWriteCount;

	if (*cLine ^ *wLine)
	{
		if (WriteAutoLogFile(PickMyStringFromIndex(114), *wLine, *cLine, conData->cMsg, conData->autoLogMaxSize))
			*wLine = *cLine;
		else
			NotifyChangeTempLogFile(conData->mProc);
	}
}

void AutoLogWriteHandler(ConsoleDat *conData)
{
	int *wrFrame = &conData->fWrFrame;
	int *appFrp = &conData->mProc->appFrame;
	int lesson = PickMyData(119);
	
	*appFrp = (*appFrp) + 1;
	if (abs(*appFrp - *wrFrame) > lesson)
	{
		AutoLogOutput(conData);
		*wrFrame = *appFrp;
	}
}

void AutoLogNop(void *arg)
{
	return;
}