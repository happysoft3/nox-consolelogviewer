
#include "setDlg.h"
#include "ctrlDraw.h"
#include "resource2.h"
#include "openAutoLog.h"
#include "noxMem.h"


void DisplayAutoLogLimit(HWND hEdit, int limit)
{
	wchar_t wstr[20] = { 0, };

	wsprintf(wstr, PickMyWideStringFromIndex(108), limit);
	SetWindowText(hEdit, wstr);
}

int GetNumberFromWChar(wchar_t *wstr, int wstrLen)
{
	char *nDest = (char *)malloc(sizeof(char) * wstrLen);
	int nRes = 0;

	memset(nDest, 0, (size_t)wstrLen);
	for (int i = 0; i < wstrLen; i++)
	{
		if (wstr[i] >= 0x30 && wstr[i] <= 0x39)
			nDest[i] = wstr[i] & 0xff;
		else if (wstr[i])
		{
			nDest[0] = 0;
			break;
		}
	}
	if (nDest[0])
		nRes = atoi(nDest);
	free(nDest);
	return nRes;
}

void ChangeAutoLogLimit(HWND hEdit, int *logLimit)
{
	wchar_t wstr[20] = { 0, };
	int wstrLen = GetWindowTextLength(hEdit);
	int num;

	if (wstrLen && wstrLen < (int)(sizeof(wstr) / sizeof(short)))
	{
		GetWindowText(hEdit, wstr, wstrLen + 1);
		num = GetNumberFromWChar(wstr, wstrLen);
		if (num && num > PickMyData(140) && num < PickMyData(141))
		{
			if (*logLimit ^ num)
				*logLimit = num;
		}
	}
}

void RenameWndNameToNumber(HWND hWnd, wchar_t *wstr, int number)
{
	wchar_t wndName[100] = { 0, };

	wsprintf(wndName, wstr, number);
	SetWindowText(hWnd, wndName);
}

int GetNumberByStaticWnd(HWND hWnd)
{
	wchar_t wndName[100] = { 0, };
	int wLen = GetWindowTextLength(hWnd) + 1, nCount = 0, cont = 1;
	short *ttt = NULL;

	GetWindowText(hWnd, wndName, wLen);
	do
	{
		ttt = wndName + wLen - 2;
		switch (*ttt)
		{
		case '0': case '1': case '2': case '3': case '4': case '5':
		case '6': case '7': case '8': case '9':
			wLen--;
			nCount++;
			break;
		default:
			cont = 0;
		}
	} while (cont);
	if (nCount)
		return _wtoi(wndName + wLen - 2);
	else
		return 0;
}

void NoxLoadTextColor(MyMainProc *mProc, HWND hDlg)
{
	int chatCol = GetMemory(mProc, 0x491c9c) & 0xff;
	int sysCol = GetMemory(mProc, 0x445515) & 0xff;
	int killCol = GetMemory(mProc, 0x495418) & 0xff;

	RenameWndNameToNumber(GetDlgItem(hDlg, IDC_STATIC3), PickMyWideStringFromIndex(142), chatCol);
	RenameWndNameToNumber(GetDlgItem(hDlg, IDC_STATIC4), PickMyWideStringFromIndex(143), sysCol);
	RenameWndNameToNumber(GetDlgItem(hDlg, IDC_STATIC5), PickMyWideStringFromIndex(148), killCol);
}

void IncreaseConTextColor(HWND hWnd, int strNum)
{
	int curNum = GetNumberByStaticWnd(hWnd);
	int selNum;

	if (curNum)
	{
		selNum = curNum + 1;
		if (selNum > 0 && selNum < 18)
		{
			RenameWndNameToNumber(hWnd, PickMyWideStringFromIndex(strNum), selNum);
		}
	}
}

void DecreaseConTextColor(HWND hWnd, int strNum)
{
	int curNum = GetNumberByStaticWnd(hWnd);
	int selNum;

	if (curNum)
	{
		selNum = curNum - 1;
		if (selNum > 0 && selNum < 18)
		{
			RenameWndNameToNumber(hWnd, PickMyWideStringFromIndex(strNum), selNum);
		}
	}
}

void ConColorSaveChange(HWND hDlg, MyMainProc *mProc)
{
	int chatCol = GetNumberByStaticWnd(GetDlgItem(hDlg, IDC_STATIC3));
	int sysCol = GetNumberByStaticWnd(GetDlgItem(hDlg, IDC_STATIC4));
	int killCol = GetNumberByStaticWnd(GetDlgItem(hDlg, IDC_STATIC5));

	if (chatCol > 0 && chatCol < 18)
		SetMemory(mProc, 0x491c9c, (GetMemory(mProc, 0x491c9c) & 0xffffff00) | chatCol);
	if (sysCol > 0 && sysCol < 18)
		SetMemory(mProc, 0x445515, (GetMemory(mProc, 0x445515) & 0xffffff00) | sysCol);
	if (killCol > 0 && killCol < 18)
		SetMemory(mProc, 0x495418, (GetMemory(mProc, 0x495418) & 0xffffff00) | killCol);
}

LRESULT CALLBACK DlgEditBoxSubClass(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;
	DlgWProc *dlgProc = (DlgWProc *)mProc->anyPoint;

	switch (msg)
	{
	case WM_RBUTTONDOWN:
		return 0;
	case WM_CHAR:
		switch (wParam)
		{
		case 8:
		case 0x30:
		case 0x31:
		case 0x32:
		case 0x33:
		case 0x34:
		case 0x35:
		case 0x36:
		case 0x37:
		case 0x38:
		case 0x39:
			break;
		default:
			return 0;
		}
	}
	return CallWindowProcA(dlgProc->dlgEditOld1, hWnd, msg, wParam, lParam);
}

void AppSettingDlgInit(HWND hDlg, MyMainProc *mProc)
{
	DlgWProc *dlgProc = (DlgWProc *)malloc(sizeof(DlgWProc));

	mProc->anyPoint = NULL;
	mProc->logPair = mProc->logPrint;

	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON1), PickMyWideStringFromIndex(125));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON2), PickMyWideStringFromIndex(124));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON3), PickMyWideStringFromIndex(121));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON4), PickMyWideStringFromIndex(133));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON5), PickMyWideStringFromIndex(134));
	SetWindowText(GetDlgItem(hDlg, IDC_STATIC2), PickMyWideStringFromIndex(110));
	DisplayAutoLogLimit(GetDlgItem(hDlg, IDC_EDIT1), mProc->conDataPtr->autoLogMaxSize);
	if (mProc->logPrint)
		SetWindowText(GetDlgItem(hDlg, IDC_STATIC1), PickMyWideStringFromIndex(122));
	else
		SetWindowText(GetDlgItem(hDlg, IDC_STATIC1), PickMyWideStringFromIndex(123));

	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON6), PickMyWideStringFromIndex(144));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON7), PickMyWideStringFromIndex(145));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON8), PickMyWideStringFromIndex(144));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON9), PickMyWideStringFromIndex(145));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON10), PickMyWideStringFromIndex(144));
	SetWindowText(GetDlgItem(hDlg, IDC_BUTTON11), PickMyWideStringFromIndex(145));
	NoxLoadTextColor(mProc, hDlg);
	mProc->anyPoint = dlgProc;
	dlgProc->dlgEditOld1 = (WNDPROC)SetWindowLong(GetDlgItem(hDlg, IDC_EDIT1), GWL_WNDPROC, (LONG)DlgEditBoxSubClass);
}

void ToggleAutoSaveLog(HWND hDlg, char *temp)
{
	*temp = *temp ^ 1;

	if (*temp)
		SetWindowText(GetDlgItem(hDlg, IDC_STATIC1), PickMyWideStringFromIndex(122));
	else
		SetWindowText(GetDlgItem(hDlg, IDC_STATIC1), PickMyWideStringFromIndex(123));
}

void OpenLogDirectory(HWND hDlg)
{
	WinExec(PickMyStringFromIndex(135), SW_SHOW);
}

void SetDlgSaveChange(MyMainProc *mProc, HWND hDlg)
{
	ConsoleDat *conData = mProc->conDataPtr;

	mProc->logPrint = mProc->logPair;
	ChangeAutoLogLimit(GetDlgItem(hDlg, IDC_EDIT1), &conData->autoLogMaxSize);
	ConColorSaveChange(hDlg, mProc);
}

LRESULT DlgEditBoxColor(MyMainProc *mProc, HDC hdc)
{
	SetTextColor(hdc, RGB(PickMyData(38), PickMyData(39), PickMyData(40)));
	SetBkColor(hdc, RGB(PickMyData(126), PickMyData(127), PickMyData(128)));

	return (LRESULT)mProc->ui_ptr->hSetDlgBrush;
}

LRESULT DlgStaticColor(MyMainProc *mProc, HDC hdc)
{
	SetTextColor(hdc, RGB(PickMyData(129), PickMyData(130), PickMyData(131)));
	SetBkColor(hdc, RGB(PickMyData(126), PickMyData(127), PickMyData(128)));

	return (LRESULT)mProc->ui_ptr->hSetDlgBrush;
}

void SettingDlgClose(HWND hDlg, MyMainProc *mProc)
{
	DlgWProc *dlgProc = (DlgWProc *)mProc->anyPoint;

	if (mProc->anyPoint != NULL)
	{
		SetWindowLong(GetDlgItem(hDlg, IDC_EDIT1), GWL_WNDPROC, (LONG)dlgProc->dlgEditOld1);
		free(mProc->anyPoint);
		mProc->anyPoint = NULL;
	}
}

BOOL CALLBACK AppSettingDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;

	switch (iMessage)
	{
	case WM_DRAWITEM:
		DrawMyDialogStaticItems((LPDRAWITEMSTRUCT)lParam);
		break;
	case WM_CTLCOLORDLG:
		return (INT_PTR)mProc->ui_ptr->hSetDlgBrush;
	case WM_INITDIALOG:
		AppSettingDlgInit(hDlg, mProc);
		return 1;
	case WM_COMMAND:
		switch (wParam)
		{
		case IDC_BUTTON1:
			SetDlgSaveChange(mProc, hDlg);
		case IDC_BUTTON2:
			SettingDlgClose(hDlg, mProc);
			EndDialog(hDlg, 0);
			break;
		case IDC_BUTTON3:
			ToggleAutoSaveLog(hDlg, &mProc->logPair);
			break;
		case IDC_BUTTON4:
			OpenLogDirectory(hDlg);
			break;
		case IDC_BUTTON5:
			DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG2), hDlg, OpenAutoLogDlgProc);
			break;
		case IDC_BUTTON6:
			IncreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC3), 142);
			break;
		case IDC_BUTTON7:
			DecreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC3), 142);
			break;
		case IDC_BUTTON8:
			IncreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC4), 143);
			break;
		case IDC_BUTTON9:
			DecreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC4), 143);
			break;
		case IDC_BUTTON10:
			IncreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC5), 148);
			break;
		case IDC_BUTTON11:
			DecreaseConTextColor(GetDlgItem(hDlg, IDC_STATIC5), 148);
			break;
		}
		break;
	case WM_CTLCOLOREDIT:
		return DlgEditBoxColor(mProc, (HDC)wParam);
	case WM_CTLCOLORSTATIC:
		return DlgStaticColor(mProc, (HDC)wParam);
	}
	return 0;
}