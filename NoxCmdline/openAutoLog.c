
#include "openAutoLog.h"
#include "general.h"
#include "resource2.h"
#include "ctrlDraw.h"

int ReadAutoLogFile(char *fName, wchar_t **dest, int logMax)
{
	FILE *fp = NULL;
	int fSize = 0;
	char *stream = NULL;
	wchar_t *wRes = NULL;

	fopen_s(&fp, fName, "rb");
	if (fp != NULL)
	{
		fSize = FileSysGetFileSize(fp);
		if (fSize && fSize < logMax)
		{
			stream = (char *)malloc(sizeof(char) * fSize + 1);
			wRes = (short *)malloc(sizeof(short) * fSize + 1);
			memset(stream, 0, (size_t)fSize + 1);
			memset(wRes, 0, (size_t)(fSize + 1) * sizeof(short));
			fread_s(stream, (size_t)fSize, (size_t)fSize, sizeof(char), fp);
			MyWideCharConvert(stream, wRes, fSize);
			free(stream);
			*dest = wRes;
		}
		fclose(fp);
	}
	return fSize;
}

void DisplayAutoLogText(HWND hEdit, HWND hStatic, int logMax)
{
	int logLen = 0;
	wchar_t *tPtr = NULL;
	wchar_t conLenStr[40] = { 0, };

	logLen = ReadAutoLogFile(PickMyStringFromIndex(114), &tPtr, logMax);
	if (tPtr != NULL)
	{
		SetWindowText(hEdit, tPtr);
		free(tPtr);
	}
	wsprintf(conLenStr, PickMyWideStringFromIndex(97), logLen);
	SetWindowText(hStatic, conLenStr);
}

LRESULT CALLBACK AutoLogDlgEditBoxSubClass(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;
	DlgWProc *dlgProc = (DlgWProc *)mProc->anyPoint;

	switch (msg)
	{
	case WM_RBUTTONDOWN:
	case WM_CHAR:
		return 0;
	}
	return CallWindowProcA(dlgProc->dlgEditOld2, hWnd, msg, wParam, lParam);
}

void AutoLogDlgInit(HWND hDlg, MyMainProc *mProc)
{
	ConsoleDat *conData = mProc->conDataPtr;
	DlgWProc *dlgProc = (DlgWProc *)mProc->anyPoint;

	dlgProc->dlgEditOld2 = (WNDPROC)SetWindowLong(GetDlgItem(hDlg, ALOG_EDIT), GWL_WNDPROC, (LONG)AutoLogDlgEditBoxSubClass);
	SetWindowText(GetDlgItem(hDlg, ALOG_BUTTON1), PickMyWideStringFromIndex(136));
	SetWindowText(GetDlgItem(hDlg, ALOG_BUTTON2), PickMyWideStringFromIndex(137));
	DisplayAutoLogText(GetDlgItem(hDlg, ALOG_EDIT), GetDlgItem(hDlg, ALOG_STATIC), conData->autoLogMaxSize);
}

int AutoLogEmpty(char *fName)
{
	return remove(fName);
}

void CloseAutoLogDlg(HWND hDlg, MyMainProc *mProc)
{
	DlgWProc *dlgProc = (DlgWProc *)mProc->anyPoint;

	SetWindowLong(GetDlgItem(hDlg, ALOG_EDIT), GWL_WNDPROC, (LONG)dlgProc->dlgEditOld2);
}

BOOL CALLBACK OpenAutoLogDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;

	switch (iMessage)
	{
	case WM_DRAWITEM:
		DrawMyDialogStaticItems((LPDRAWITEMSTRUCT)lParam);
		break;
	case WM_CTLCOLORDLG:
		return (INT_PTR)mProc->ui_ptr->hSetDlgBrush;
	case WM_INITDIALOG:
		AutoLogDlgInit(hDlg, mProc);
		return 1;
	case WM_COMMAND:
		switch (wParam)
		{
		case ALOG_BUTTON1:
			AutoLogEmpty(PickMyStringFromIndex(114));
		case ALOG_BUTTON2:
			EndDialog(hDlg, 0);
		}
		break;
	case WM_CTLCOLORSTATIC:
		return DlgEditColor(mProc, (HDC)wParam);
	}
	return 0;
}