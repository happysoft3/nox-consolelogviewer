
#include "head.h"
#include "nox_proc.h"
#include "app_gui.h"
#include "listViewHandler.h"
#include "ctrlDraw.h"
#include "LogOutput.h"
#include "consoleLog.h"


LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;

	switch (iMessage)
	{
	case WM_CREATE:
		InitProcessMask(mProc);
		StartupMessage(mProc, hWnd);
		break;
	case WM_DESTROY:
		ExitApplication(mProc);
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		DrawingSomething(mProc, hWnd);
		break;
	case WM_CLOSE:
		CloseApplication(mProc, hWnd);
		break;
	case WM_SETFOCUS:
		FocusSetToEdit(mProc);
		break;
	case WM_DRAWITEM:
		OnDrawItem(hWnd, (DRAWITEMSTRUCT *)lParam);
		break;
	case WM_NOTIFY:
		return MapListViewOnDisplay(hWnd, mProc, lParam);
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case PUS_BUT1:
			ExecCmdLineWrite(mProc, hWnd);
			break;
		case PUS_BUT2:
			ConvenientChatButton(mProc);
			break;
		case PUS_BUT3:
			OpenAppSetting(hWnd, mProc);
			break;
		case PUS_BUT4:
			break;
		case SUB_BUTN1:
			ReConnection(mProc, hWnd);
			SpecificRedrawLocation(hWnd);
			break;
		case SUB_BUTN2:
			switch (OutputConsoleLog(mProc->conDataPtr, mProc->conLineCount))
			{
			case 0:
				PrintStatus(mProc, PickMyWideStringFromIndex(98));
				break;
			case 1:
				PrintStatus(mProc, PickMyWideStringFromIndex(93));
				break;
			case 2:
				PrintStatus(mProc, PickMyWideStringFromIndex(94));
			}
			break;
		case SUB_BUTN3:
			ResetConsoleLog(mProc, hWnd);
		}
		break;
	case WM_CTLCOLORSTATIC:
		return StaticCustomBrush(mProc, (HDC)wParam);
	}
	return (DefWindowProc(hWnd, iMessage, wParam, lParam));
}

LRESULT CALLBACK EditBoxSubClass(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;

	switch (msg)
	{
	case WM_RBUTTONDOWN:
		return 0;
	case WM_CHAR: //10
		switch (wParam)
		{
		case 10:
			SetFocus(mProc->ui_ptr->butn2);
			SendCommandEnter(mProc, hWnd);
			return 0;
		case VK_RETURN:
			SetFocus(mProc->ui_ptr->butn2);
			ChatEnter(mProc);
			return 0;
		}
	}
	return CallWindowProcA(g_oldEditProc, hWnd, msg, wParam, lParam);
}