
#include "patalErrCheck.h"


int PatalErrorCheck()
{
	wchar_t wCon[200] = { 0, }, wCap[200] = { 0, };
	//TODO: 언어 파일을 먼저 읽어들입니다
	if (ReadLanguageFile("NoxCmdCore\\NoxCmdDump.bmp", PROC_MASK))
	{
		//TODO: 동일한 프로세스가 있는지 검사합니다
		if (PROC_MASK->existProcErr)
		{
			//MyQuestionBox(NULL, 1, 3, MB_OK | MB_ICONERROR);
			memcpy_s(wCon, sizeof(wCon), PickMyWideStringFromIndex(112), sizeof(wCon));
			memcpy_s(wCap, sizeof(wCap), PickMyWideStringFromIndex(113), sizeof(wCap));
			ClearStringDataFromMemory(PROC_MASK);
			MessageBox(NULL, wCon, wCap, MB_ICONERROR | MB_OK);
			return 0;
		}
		else
			return 1;
	}
	return 0;
}