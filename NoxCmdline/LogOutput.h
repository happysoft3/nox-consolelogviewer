#pragma once

#include "head.h"

int OutputConsoleLog(ConsoleDat *conData, int logCount);
void AutoLogWriteHandler(ConsoleDat *conData);
void AutoLogNop(void *arg);