
#include "pThread.h"
#include "noxMem.h"
#include "consoleLog.h"
#include "listViewHandler.h"
#include "app_gui.h"
#include <process.h>

void SubPartLoop(ConsoleDat *conData)
{
	FuncPtr *logWrFunc = &conData->fPtr;

	(*logWrFunc)(conData);
}

void NoxMessageLoop(PMASK *pmask)
{
	int pCapture = GetConsoleLogLastest(pmask);

	if (pCapture)
	{
		if (!(GetMemory(pmask, pCapture + 512)))
		{
			ConsoleLogHandler(pmask, pCapture);
			UpdateConsoleLogList(pmask->ui_ptr->conList, pmask->conLineCount, &pmask->listSel);
		}
	}
	SubPartLoop(pmask->conDataPtr);
}

unsigned WINAPI NoxMessageLoopThread(MyMainProc *mProc)
{
	while (1)
	{
		if (mProc->subThreadExit)
			break;
		if (IsNoxProcessOn(mProc))
		{
			if (mProc->noxMsgLoop)
				NoxMessageLoop(mProc);
			else
			{
				mProc->noxMsgLoop = 1;
				CapturePrevLogMessageHandler(mProc);
			}
		}
		else if (mProc->nox_is_exec)
		{
			mProc->nox_is_exec = 0;
			SpecificRedrawLocation(mProc->ui_ptr->mainWnd);
		}
		Sleep(100);
	}
	mProc->subThreadEnd = 1;
	return 0;
}

int CreateNoxThread(MyMainProc *mProc)
{
	DWORD pid = 0;
	HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, NoxMessageLoopThread, mProc, 0, &pid);

	mProc->subThreadExit = 0;
	mProc->subThreadEnd = 0;
	if (hThread)
	{
		mProc->subThreadStart = 1;
	}
	else
	{
		mProc->subThreadStart = 0;
	}
	return mProc->subThreadStart;
}