
#include "chatWnd.h"
#include "resource2.h"
#include "ctrlDraw.h"

void ChatWndClassInit(HWND hDlg)
{
	SetWindowText(GetDlgItem(hDlg, MSG_LOG_OK), PickMyWideStringFromIndex(137));
}

BOOL CALLBACK ChatWndDlgProc(HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	MyMainProc *mProc = PROC_MASK;

	switch (iMessage)
	{
	case WM_DESTROY:
		return 0;
	case WM_ACTIVATE:
		if (LOWORD(wParam) == WA_INACTIVE)
			ShowWindow(hDlg, SW_HIDE);
		return 0;
	case WM_DRAWITEM:
		OnDrawItem(hDlg, (DRAWITEMSTRUCT *)lParam);
		break;
	case WM_INITDIALOG:
		ChatWndClassInit(hDlg);
		return 1;
	case WM_COMMAND:
		switch (wParam)
		{
		case MSG_LOG_OK:
			ShowWindow(hDlg, SW_HIDE);
		}
		break;
	case WM_CTLCOLORDLG:
		return (INT_PTR)mProc->ui_ptr->hSetDlgBrush;
	case WM_CTLCOLORSTATIC:
		return DlgEditColor(mProc, (HDC)wParam);
	}
	return 0;
}

void* CreateMyChatWindow(HINSTANCE hInst, HWND hWnd)
{
	HWND cWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG3), hWnd, ChatWndDlgProc);

	ShowWindow(cWnd, SW_HIDE);
	return cWnd;
}

int GetLenConsoleMessage(char *cMsg)
{
	int max = 508 / 2;

	for (int i = 0; i < max - 2; i += 2)
	{
		if (cMsg[i] || cMsg[i + 1])
			continue;
		else
			return i;
	}
	return 0;
}

int ParseMessageData(char *content, wchar_t **wDest, int dateTime)
{
	wchar_t *picDesc = PickMyWideStringFromIndex(149);
	int srcLen = GetLenConsoleMessage(content), descLen = PickMyStringLenFromIndex(149) * 2 + 1;
	wchar_t *wTemp = (wchar_t *)malloc(sizeof(short) * (srcLen + 1 + descLen));
	int sec = dateTime & 0xff, minute = (dateTime >> 0x08) & 0xff, hour = (dateTime >> 0x10) & 0xff;

	memset(wTemp, 0, (size_t)(srcLen + descLen + 1));
	wsprintf(wTemp, picDesc, hour, minute, sec, (wchar_t *)content);
	*wDest = wTemp;
}

void UpdateChatWnd(HWND hDlg, char *content, int dateTime)
{
	wchar_t *wDest = NULL;

	ParseMessageData(content, &wDest, dateTime);
	if (wDest != NULL)
	{
		SetWindowText(GetDlgItem(hDlg, MSG_LOG_STATIC1), wDest);
		free(wDest);
		ShowWindow(hDlg, SW_SHOW);
	}
}