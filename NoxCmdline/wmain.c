
#include "head.h"
#include "nox_proc.h"
#include "patalErrCheck.h"

LPCWSTR lpszClass = L"�콺 ����â";

LPCWSTR MutexSeqID = L"NOX CMDLINE //TODO: Happy Soft LTD --";

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	UI_SET ui_st;
	PMASK pmask;
	HWND hWnd;
	HANDLE hMutex = CreateMutex(NULL, TRUE, MutexSeqID);
	MSG Message;
	WNDCLASS WndClass;

	PROC_MASK = &pmask;
	g_hInst = hInstance;
	pmask.ui_ptr = &ui_st;
	pmask.existProcErr = (GetLastError() == ERROR_ALREADY_EXISTS);
	if (!PatalErrorCheck())
		return 0;

	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hbrBackground = (HBRUSH)GetStockObject(3);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hInstance = hInstance;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.lpszClassName = lpszClass;
	WndClass.lpszMenuName = NULL;
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&WndClass);

	hWnd = CreateWindow(lpszClass, lpszClass, WS_CAPTION | WS_SYSMENU, //WS_OVERLAPPEDWINDOW WS_VSCROLL,
		CW_USEDEFAULT, 0, 930, 500, NULL, (HMENU)NULL, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);
	SetWindowText(hWnd, PickMyWideStringFromIndex(0));

	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
	return Message.wParam;
}