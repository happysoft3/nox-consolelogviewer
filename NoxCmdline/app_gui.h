#pragma once

#include "head.h"

void StartupMessage(PMASK *pmask, HWND hWnd);
void DrawingSomething(PMASK *pmask, HWND hWnd);
void SpecificRedrawLocation(HWND hWnd);
void DisplayNoxStatus(PMASK *pmask, HDC *h_ptr);
void OpenAppSetting(HWND hWnd, MyMainProc *mProc);