#pragma once


#include "head.h"
#include "noxMem.h"

void ConsoleHandleInit(ConsoleDat *conData);
void ConsoleLogMessageAllClearOnMemory(ConsoleDat *conData);
void ResetConsoleLog(MyMainProc *mProc, HWND hWnd);
int GetConsoleLogAddress(MyMainProc *mProc);
int GetConsoleLogLastest(MyMainProc *mProc);
void CapturePrevLogMessageHandler(MyMainProc *mProc);
void ConsoleLogHandler(MyMainProc *mProc, int pCap);