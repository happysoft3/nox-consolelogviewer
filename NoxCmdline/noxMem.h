#pragma once

#include "head.h"

#define TEST_CHECK_ADDR			0x005babd0


int GetMemory(PMASK *pmask, int target);
float GetFloatMemory(PMASK *pmask, int target);
int ReadMemString(PMASK *pmask, int ptr, char *buf, int len);
void SetMemory(PMASK *pmask, int target, int value);
void WriteMemString(PMASK *pmask, int target, char *buf, int len);

int IsNoxProcessOn(PMASK *pmask);
int NoxCurrentFrames(PMASK *pmask);
int IsInGameMode(PMASK *pmask);