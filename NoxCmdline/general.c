
#include "general.h"

int FileSysGetFileSize(FILE *fp)
{
	int temp = ftell(fp), res = 0;

	fseek(fp, 0, SEEK_END);
	res = ftell(fp);
	fseek(fp, 0, temp);
	return res;
}

void MyWideCharConvert(char *src, wchar_t *dest, int len)
{
	//wchar_t wStr[200] = { 0, };
	int nLen = MultiByteToWideChar(CP_ACP, 0, src, strlen(src), NULL, 0);

	MultiByteToWideChar(CP_ACP, 0, src, len, dest, nLen);
	//memcpy_s(dest, len * 2, wStr, len * 2 - 2);
}

void MyBufferRealloc(char **dest, int memSize)
{
	char *buff = (char *)malloc(sizeof(char) * (memSize));

	memset(buff, 0, (size_t)memSize);
	*dest = buff;
}