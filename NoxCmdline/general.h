#pragma once

#include "head.h"

int FileSysGetFileSize(FILE *fp);
void MyWideCharConvert(char *src, wchar_t *dest, int len);
void MyBufferRealloc(char **dest, int memSize);