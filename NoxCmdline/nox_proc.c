
#include "head.h"
#include "nox_proc.h"
#include "nox_func.h"
#include "pThread.h"
#include "noxMem.h"


#include "consoleLog.h"


void InitProcessMask(PMASK *pmask)
{
	ConsoleDat *conData = (ConsoleDat *)malloc(sizeof(ConsoleDat));

	pmask->logPrint = PickMyData(115);

	pmask->appFrame = 0;
	pmask->appStart = 1;
	conData->autoLogMaxSize = PickMyData(139);
	conData->mProc = pmask;
	conData->conLimit = PickMyData(138);
	ConsoleHandleInit(conData);
	pmask->conLineCount = 0;
	pmask->conAddr = 0;
	pmask->conDataPtr = conData;
	pmask->hwnd = NULL;
	pmask->process_id = 0;
	pmask->pground = NULL;
	pmask->nox_is_exec = SearchProcessID(pmask);
	CreateNoxThread(pmask);
	if (pmask->nox_is_exec)
	{
		pmask->cur_frp = 0;
	}
}

HWND SearchFirstNoxProcess(char *procName)
{
	HWND curWnd = FindWindowA(NULL, procName), resWnd = NULL;

	while (curWnd != NULL)
	{
		resWnd = curWnd;
		curWnd = FindWindowA(NULL, procName);
	}
	return resWnd;
}

int SearchProcessID(PMASK *pmask)
{
	pmask->hwnd = FindWindowA(NULL, PickMyStringFromIndex(12));

	if (pmask->hwnd)
	{
		GetWindowThreadProcessId(pmask->hwnd, &pmask->process_id);
		pmask->pground = OpenProcess(MAXIMUM_ALLOWED, 0, pmask->process_id);
		if (pmask->pground)
		{
			pmask->noxMsgLoop = 0;
			return 1;
		}
	}
	return 0;
}

void CloseApplication(PMASK *pmask, HWND hWnd)
{
	DestroyWindow((HWND)pmask->chatViewDlg);
	DeleteObject(pmask->ui_ptr->hListBrush);
	DeleteObject(pmask->ui_ptr->hStaticBrush);
	DeleteObject(pmask->ui_ptr->hSetDlgBrush);
	DeleteObject(pmask->ui_ptr->font1);
	DeleteObject(pmask->ui_ptr->font2);
	SetWindowLong(pmask->ui_ptr->hEdit, GWL_WNDPROC, (LONG)g_oldEditProc);
}

void ExitApplication(PMASK *pmask)
{
	if (pmask->nox_is_exec)
		CloseHandle(pmask->pground);
	ClearStringDataFromMemory(pmask);
	if (pmask->appStart)
	{
		ConsoleLogMessageAllClearOnMemory(pmask->conDataPtr);
		free(pmask->conDataPtr);
	}
	if (pmask->subThreadStart)
	{
		pmask->subThreadExit = 1;
		while (1)
		{
			if (pmask->subThreadEnd)
				break;
			Sleep(2);
		}
	}
}

void ExecCmdLineWrite(PMASK *pmask, HWND hWnd)
{
	wchar_t wstr[200] = { 0, };
	DWORD frs;

	if (IsNoxProcessOn(pmask))
	{
		if (IsInGameMode(pmask))
		{
			frs = GetMemory(pmask, CURRENT_FRAME);
			if (abs(frs - pmask->cur_frp) > COOL_DOWN_VALUE)
			{
				GetWindowText(pmask->ui_ptr->hEdit, wstr, sizeof(wstr));
				SetWindowText(pmask->ui_ptr->hEdit, L"");
				NoxCmdLine(pmask, wstr);
				pmask->cur_frp = frs;
				PrintStatus(pmask, PickMyWideStringFromIndex(64));
			}
			else
			{
				PrintStatus(pmask, PickMyWideStringFromIndex(65));
			}
		}
		else
		{
			PrintStatus(pmask, PickMyWideStringFromIndex(66));
			MessageBox(hWnd, PickMyWideStringFromIndex(67), PickMyWideStringFromIndex(68), MB_OK);
		}
	}
	else
		PrintStatus(pmask, PickMyWideStringFromIndex(69));
}

void ConvenientChatButton(PMASK *pmask)
{
	wchar_t wstr[200] = { 0, }, cmds[208] = { 0, };

	if (GetWindowTextLength(pmask->ui_ptr->hEdit))
	{
		if (IsNoxProcessOn(pmask))
		{
			if (IsInGameMode(pmask))
			{
				GetWindowText(pmask->ui_ptr->hEdit, wstr, sizeof(wstr));
				SetWindowText(pmask->ui_ptr->hEdit, L"");
				wsprintf(cmds, L"say %s", wstr);
				NoxCmdLine(pmask, cmds);
				PrintStatus(pmask, PickMyWideStringFromIndex(71));
			}
			else
				PrintStatus(pmask, PickMyWideStringFromIndex(72));
		}
		else
			PrintStatus(pmask, PickMyWideStringFromIndex(73));
	}
}

void ChatEnter(PMASK *pmask)
{
	ConvenientChatButton(pmask);
	SetFocus(pmask->ui_ptr->hEdit);
}

void SendCommandEnter(PMASK *pmask, HWND hWnd)
{
	ExecCmdLineWrite(pmask, hWnd);
	SetFocus(pmask->ui_ptr->hEdit);
}

void PrintStatus(PMASK *pmask, wchar_t *wstr)
{
	SetWindowText(pmask->ui_ptr->hEditOut, wstr);
}

void SetCoopTeamMode(PMASK *pmask, HWND hWnd)
{
	DWORD check;

	if (ReadProcessMemory(pmask->pground, (LPCVOID)CHECK_IN_GAME, &check, sizeof(DWORD), NULL))
	{
		if (check)
		{
			CreateCoopTeam(pmask, L"a");
		}
		//MessageBox(hWnd, L"협동 팀 모드로 설정되었습니다", L"알림", MB_OK);
	}
}

void FocusSetToEdit(PMASK *pmask)
{
	SetFocus(pmask->ui_ptr->hEdit);
}

void ReConnection(PMASK *pmask, HWND hWnd)
{
	if (IsNoxProcessOn(pmask))
		PrintStatus(pmask, PickMyWideStringFromIndex(85));
	else
	{
		pmask->nox_is_exec = SearchProcessID(pmask);
		if (pmask->nox_is_exec)
		{
			pmask->cur_frp = 0;
			PrintStatus(pmask, PickMyWideStringFromIndex(86));
		}
		else
			PrintStatus(pmask, PickMyWideStringFromIndex(92));
	}
}
