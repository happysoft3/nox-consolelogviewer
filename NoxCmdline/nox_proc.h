#pragma once

#include "head.h"

#define CHECK_IN_GAME									0x6d8530
#define CURRENT_FRAME									0x84ea04
#define COOL_DOWN_VALUE									150

void InitProcessMask(PMASK *pmask);
int SearchProcessID(PMASK *pmask);
void CloseApplication(PMASK *pmask, HWND hWnd);
void ExitApplication(PMASK *pmask);
void ExecCmdLineWrite(PMASK *pmask, HWND hWnd);
void ConvenientChatButton(PMASK *pmask);
void ToggleAllowEnterKey(PMASK *pmask, HWND hWnd);
void ToggleMoveLastLine(PMASK *pmask, HWND hWnd);
void ChatEnter(PMASK *pmask);
void SendCommandEnter(PMASK *pmask, HWND hWnd);
void PrintStatus(PMASK *pmask, wchar_t *wstr);
void SetCoopTeamMode(PMASK *pmask, HWND hWnd);
void FocusSetToEdit(PMASK *pmask);
void ReConnection(PMASK *pmask, HWND hWnd);
