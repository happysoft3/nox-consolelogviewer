#pragma once

#include "head.h"

#define CON_COLOR											0x69fe50
#define CON_STR_ADDR										0x69c9c8
//#define CON_SAY_ADDR										0x751000

typedef unsigned int(__stdcall *ThreadFuncPtr)(void *);

typedef struct _nox_cmd
{
	wchar_t msg[200];
	ThreadFuncPtr nox_func_addr;
}NX_CMD;

void NoxConsolePrint(PMASK *pmask, char *str, int color);

unsigned WINAPI ThreadNoxCmdLine(void *thread_ptr);
void NoxCmdLine(PMASK *pmask, wchar_t *cmd);
unsigned WINAPI ThreadCreateCoopTeam(void *thread_ptr);
void CreateCoopTeam(PMASK *pmask, wchar_t *cmd);

