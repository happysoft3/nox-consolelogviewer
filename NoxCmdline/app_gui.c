
#include "head.h"
#include "app_gui.h"
#include "ListviewUi.h"
#include "setDlg.h"
#include "LogOutput.h"
#include "resource2.h"

#include "chatWnd.h"


void CreateMyBrushObject(UI_SET *uig)
{
	//TODO: ����Ʈ �귯��
	uig->hListBrush = CreateSolidBrush(RGB(PickMyData(44), PickMyData(45), PickMyData(46)));
	//TODO: ����ƽ �귯��
	uig->hStaticBrush = CreateSolidBrush(RGB(PickMyData(47), PickMyData(48), PickMyData(49)));
	uig->hSetDlgBrush = CreateSolidBrush(RGB(PickMyData(126), PickMyData(127), PickMyData(128)));
}

void CreateSubButtons(HWND hWnd, HFONT btFont)
{
	HWND subButn1 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(83), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(57), PickMyData(84), PickMyData(60), PickMyData(90), hWnd, (HMENU)SUB_BUTN1, g_hInst, NULL);
	HWND subButn2 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(87), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(57) - PickMyData(91), PickMyData(52), PickMyData(60), PickMyData(90), hWnd, (HMENU)SUB_BUTN2, g_hInst, NULL);
	HWND subButn3 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(101), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(57) - PickMyData(91), PickMyData(102), PickMyData(60), PickMyData(90), hWnd, (HMENU)SUB_BUTN3, g_hInst, NULL);
	HWND subButn4 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(95), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(57), PickMyData(96), PickMyData(60), PickMyData(90), hWnd, (HMENU)PUS_BUT3, g_hInst, NULL);

	SendMessage(subButn1, WM_SETFONT, (WPARAM)btFont, 1);
	SendMessage(subButn2, WM_SETFONT, (WPARAM)btFont, 1);
	SendMessage(subButn3, WM_SETFONT, (WPARAM)btFont, 1);
	SendMessage(subButn4, WM_SETFONT, (WPARAM)btFont, 1);
}

void StartupMessage(PMASK *pmask, HWND hWnd)
{
	UI_SET *uig = pmask->ui_ptr;

	uig->butn1 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(1), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(51), PickMyData(52), PickMyData(55), PickMyData(56), hWnd, (HMENU)PUS_BUT1, g_hInst, NULL);
	uig->butn2 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(2), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
		PickMyData(53), PickMyData(54), PickMyData(55), PickMyData(56), hWnd, (HMENU)PUS_BUT2, g_hInst, NULL);
	//uig->butn3 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(3), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
	//	PickMyData(57), PickMyData(58), PickMyData(60), PickMyData(90), hWnd, (HMENU)PUS_BUT3, g_hInst, NULL);
	//uig->butn4 = CreateWindow(WC_BUTTON, PickMyWideStringFromIndex(50), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
	//	PickMyData(57), PickMyData(59), PickMyData(60), PickMyData(90), hWnd, (HMENU)PUS_BUT4, g_hInst, NULL);
	pmask->ui_ptr->font1 = CreateFont(15, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1,
		VARIABLE_PITCH | FF_ROMAN, L"���� ����");
	pmask->ui_ptr->font2 = CreateFont(12, 0, 0, 0, 0, 0, 0, 0, HANGEUL_CHARSET, 3, 2, 1,
		VARIABLE_PITCH | FF_ROMAN, L"����");
	pmask->ui_ptr->hEdit = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_MULTILINE | ES_WANTRETURN,
		30, PickMyData(28), PickMyData(23), PickMyData(26), hWnd, (HMENU)EDIT_ID, g_hInst, NULL);
	pmask->ui_ptr->hEditOut = CreateWindow(WC_STATIC, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER,
		30, PickMyData(29), PickMyData(24), PickMyData(26), hWnd, (HMENU)EDIT_ID, g_hInst, NULL);

	uig->mainWnd = hWnd;
	ConsoleLogListview(pmask, hWnd);
	SendMessage(pmask->ui_ptr->hEdit, WM_SETFONT, (WPARAM)pmask->ui_ptr->font2, 1);
	SendMessage(pmask->ui_ptr->hEditOut, WM_SETFONT, (WPARAM)pmask->ui_ptr->font2, 1);
	SendMessage(uig->butn1, WM_SETFONT, (WPARAM)pmask->ui_ptr->font1, 1);
	SendMessage(uig->butn2, WM_SETFONT, (WPARAM)pmask->ui_ptr->font1, 1);
	//SendMessage(uig->butn3, WM_SETFONT, (WPARAM)pmask->ui_ptr->font1, 1);
	//SendMessage(uig->butn4, WM_SETFONT, (WPARAM)pmask->ui_ptr->font1, 1);
	g_oldEditProc = (WNDPROC)SetWindowLong(pmask->ui_ptr->hEdit, GWL_WNDPROC, (LONG)EditBoxSubClass);
	CreateSubButtons(hWnd, uig->font1);
	CreateMyBrushObject(uig);
	pmask->chatViewDlg = (HWND)CreateMyChatWindow(g_hInst, hWnd);
}

void DrawingSomething(PMASK *pmask, HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	HFONT org_font = (HFONT)SelectObject(hdc, pmask->ui_ptr->font1);

	SetTextColor(hdc, RGB(153, 213, 217));
	SetBkColor(hdc, RGB(64, 64, 64));
	TextOut(hdc, PickMyData(30), PickMyData(31), PickMyWideStringFromIndex(5), PickMyStringLenFromIndex(5));
	DisplayNoxStatus(pmask, &hdc);
	SelectObject(hdc, org_font);
	EndPaint(hWnd, &ps);
}

void SpecificRedrawLocation(HWND hWnd)
{
	RECT redrawRect1 = { PickMyData(61), PickMyData(31), PickMyData(61) + PickMyData(78), PickMyData(31) + PickMyData(79) };

	InvalidateRect(hWnd, &redrawRect1, 1);
}

void DisplayNoxStatus(PMASK *pmask, HDC *h_ptr)
{
	SetBkColor(*h_ptr, RGB(64, 64, 64));
	if (pmask->nox_is_exec)
	{
		SetTextColor(*h_ptr, RGB(0, 225, 32));
		TextOut(*h_ptr, PickMyData(61), PickMyData(31) + 20, PickMyWideStringFromIndex(10), PickMyStringLenFromIndex(10));
	}
	else
	{
		SetTextColor(*h_ptr, RGB(225, 0, 16));
		TextOut(*h_ptr, PickMyData(61), PickMyData(31) + 20, PickMyWideStringFromIndex(11), PickMyStringLenFromIndex(11));
	}
}

void OpenAppSetting(HWND hWnd, MyMainProc *mProc)
{
	ConsoleDat *conData = mProc->conDataPtr;

	DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, AppSettingDlgProc);
	if (mProc->logPrint)
		conData->fPtr = AutoLogWriteHandler;
	else
		conData->fPtr = AutoLogNop;
}