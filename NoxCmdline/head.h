#pragma once

#include <conio.h>
#include <Windows.h>
#include <stdio.h>
#include <CommCtrl.h>

#define EDIT_ID								1
#define PUS_BUT1							2
#define PUS_BUT2							3
#define PUS_BUT3							4
#define PUS_BUT4							5
#define CON_LOG_LIST						6

#define SUB_BUTN1							7
#define SUB_BUTN2							8
#define SUB_BUTN3							9

WNDPROC g_oldEditProc;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK EditBoxSubClass(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

HINSTANCE g_hInst;

typedef void (*FuncPtr)(void *);

typedef struct _ui_info
{
	HWND butn1;
	HWND butn2;
	HWND hEdit;
	HWND hEditOut;
	HFONT font1;
	HFONT font2;
	HWND conList;
	HBRUSH hListBrush;
	HBRUSH hStaticBrush;
	HBRUSH hSetDlgBrush;
	HWND mainWnd;
} UI_SET;

typedef struct _my_string
{
	int stringLen;
	char *stringData;
	wchar_t *wStringData;
}MyStringList;

typedef struct _conMsg
{
	int chkSum;
	char content[508];
	int time;
	int color;
	int endSig;
}ConsoleMsg;

typedef struct _conData ConsoleDat;

typedef struct _dlgWndProc
{
	WNDPROC dlgEditOld1;
	WNDPROC dlgEditOld2;
}DlgWProc;

typedef struct _processMask
{
	unsigned char nox_is_exec;
	unsigned char appStart;

	unsigned char logPrint;
	unsigned char logPair;

	DWORD cur_frp;
	HWND hwnd;
	HANDLE pground;
	HANDLE noxPthread;
	DWORD process_id;
	struct _ui_info *ui_ptr;
	char existProcErr;

	int subThreadExit;
	int subThreadEnd;
	int subThreadStart;
	int allStringCount;
	char nullStringData[8];
	MyStringList *myStringPic;

	int appFrame;
	int conAddr;
	int conLineCount;
	int listSel;
	struct _conData *conDataPtr;

	unsigned char noxMsgLoop;
	void *anyPoint;
	void *chatViewDlg;
} PMASK;

typedef PMASK MyMainProc;

typedef struct _conData
{
	MyMainProc *mProc;
	int conMsgCount;
	int curAddr;
	int conLimit;
	int updateTime;
	struct _conMsg **cMsg;
	int fWriteCount;
	int fWrFrame;
	FuncPtr fPtr;
	int autoLogMaxSize;
}ConsoleDat;


PMASK *PROC_MASK;

#include "appLang.h"