
#include "consoleLog.h"
#include "listViewHandler.h"
#include "LogOutput.h"
#include "nox_func.h"

void ConsoleHandleInit(ConsoleDat *conData)
{
	conData->conMsgCount = 0;
	conData->curAddr = 0;
	conData->updateTime = 0;
	conData->cMsg = (ConsoleMsg **)malloc(sizeof(int) * conData->conLimit);

	conData->fWriteCount = 0;
	if (conData->mProc->logPrint)
		conData->fPtr = AutoLogWriteHandler;
	else
		conData->fPtr = AutoLogNop;
	conData->fWrFrame = 0;
}

void ConsoleLogMessageAllClearOnMemory(ConsoleDat *conData)
{
	int msgCount = conData->conMsgCount;

	conData->conMsgCount = 0;
	conData->mProc->conLineCount = 0;
	for (int i = 0; i < msgCount; i++)
		free(conData->cMsg[i]);
	free(conData->cMsg);
}

void ResetConsoleLog(MyMainProc *mProc, HWND hWnd)
{
	if (MessageBox(hWnd, PickMyWideStringFromIndex(104), PickMyWideStringFromIndex(105), MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		ConsoleLogMessageAllClearOnMemory(mProc->conDataPtr);
		ConsoleHandleInit(mProc->conDataPtr);
		mProc->conLineCount = 0;
		UpdateConsoleLogList(mProc->ui_ptr->conList, mProc->conLineCount, &mProc->listSel);
	}
}

int GetConsoleLogAddress(MyMainProc *mProc)
{
	if (GetMemory(mProc, 0x69fe4c))
	{
		return GetMemory(mProc, GetMemory(mProc, GetMemory(mProc, 0x69fe4c) + 32) + 24);
	}
	return 0;
}

int GetConsoleLogLineCount(MyMainProc *mProc)
{
	if (GetMemory(mProc, 0x69fe4c))
	{
		return GetMemory(mProc, GetMemory(mProc, GetMemory(mProc, 0x69fe4c) + 32) + 44) & 0xff;
	}
	return 0;
}

int GetConsoleLogLastest(MyMainProc *mProc)
{
	int pCap = GetConsoleLogAddress(mProc), latest = 0, lCount;

	if (pCap)
	{
		lCount = GetConsoleLogLineCount(mProc);
		if (lCount)
			latest = pCap + ((lCount - 1) * sizeof(ConsoleMsg));
	}
	return latest;
}

int GetMySysTime()
{
	SYSTEMTIME sTime;

	GetLocalTime(&sTime);

	int hour = sTime.wHour, min = sTime.wMinute, sec = sTime.wSecond;
	return sec | (min << 8) | (hour << 16) | (1 << 24);
}

void ConsoleLogUpdate(int addr, ConsoleDat *conData, int idx, int time)
{
	ConsoleMsg **conMsg = conData->cMsg;

	if (!(GetMemory(conData->mProc, addr + 512)))
		SetMemory(conData->mProc, addr + 512, time);
	ReadMemString(conData->mProc, addr, (char *)&conMsg[idx], sizeof(conMsg[0]));
}

void ConsoleLogUpdateAll(ConsoleDat *conData, int cAddr)
{
	int logTime;
	int cCount = GetConsoleLogLineCount(conData->mProc);

	if (cCount)
	{
		logTime = GetMySysTime();
		for (int i = 0; i < cCount; i++)
			ConsoleLogUpdate(cAddr + ((int)sizeof(conData->cMsg[0]) * i), conData, i, logTime);
	}
}

void RearrageConsoleLog(ConsoleDat *conData)
{
	int msgCount = conData->conMsgCount;

	free(conData->cMsg[0]);
	for (int i = 1; i < msgCount; i++)
		conData->cMsg[i - 1] = conData->cMsg[i];
}

int CheckLogLimit(ConsoleDat *conData)
{
	int *curCount = &conData->conMsgCount;
	int limit = conData->conLimit;

	if (*curCount < limit)
		return 0;

	return 1;
}

void InsertNewLogMessage(ConsoleDat *conData, int pCur, char writeTime)
{
	ConsoleMsg *cMsg = (ConsoleMsg *)malloc(sizeof(ConsoleMsg));

	if (writeTime)
		SetMemory(conData->mProc, pCur + 512, conData->updateTime);
	ReadMemString(conData->mProc, pCur, (char *)cMsg, sizeof(ConsoleMsg));
	if (CheckLogLimit(conData))
		RearrageConsoleLog(conData);
	else
		conData->conMsgCount++;
	conData->cMsg[conData->conMsgCount - 1] = cMsg;
}

int InterceptPreviousLogMessage(ConsoleDat *conData, int pCur, int logCount)
{
	if (GetMemory(conData->mProc, pCur + 512) && logCount)
	{
		InsertNewLogMessage(conData, pCur, 0);
		return InterceptPreviousLogMessage(conData, pCur + sizeof(ConsoleMsg), logCount - 1) + 1;
	}
	else
		return 0;
}

void CapturePrevLogMessageHandler(MyMainProc *mProc)
{
	ConsoleDat *conData = mProc->conDataPtr;
	int count = InterceptPreviousLogMessage(conData, GetConsoleLogAddress(mProc), GetConsoleLogLineCount(mProc));

	if (count && IsInGameMode(mProc))
		NoxConsolePrint(mProc, PickMyStringFromIndex(132), 13);
}

int InterceptLogMessage(ConsoleDat *conData, int pCur)
{
	int count;

	if (GetMemory(conData->mProc, pCur + 512))
		return 0;
	else
	{
		if (pCur > conData->curAddr)
			count = InterceptLogMessage(conData, pCur - sizeof(ConsoleMsg)) + 1;
		InsertNewLogMessage(conData, pCur, 1);
		return count;
	}
}

void ConsoleLogHandler(MyMainProc *mProc, int pCap)
{
	ConsoleDat *conData = mProc->conDataPtr;

	conData->curAddr = GetConsoleLogAddress(mProc);
	conData->updateTime = GetMySysTime();
	InterceptLogMessage(conData, pCap);
	mProc->conLineCount = conData->conMsgCount;
}