
#include "noxMem.h"

int GetMemory(PMASK *pmask, int target)
{
	int res = 0;

	if (!ReadProcessMemory(pmask->pground, (LPCVOID)target, (LPVOID)&res, sizeof(DWORD), NULL))
		return 0;
	return res;
}

float GetFloatMemory(PMASK *pmask, int target)
{
	float res = 0;

	if (!ReadProcessMemory(pmask->pground, (LPCVOID)target, (LPVOID)&res, sizeof(DWORD), NULL))
		return 0;
	return res;
}

int ReadMemString(PMASK *pmask, int ptr, char *buf, int len)
{
	return ReadProcessMemory(pmask->pground, (LPCVOID)ptr, (LPVOID)buf, (size_t)len, NULL);
}

void SetMemory(PMASK *pmask, int target, int value)
{
	WriteProcessMemory(pmask->pground, (LPVOID)target, (LPCVOID)&value, sizeof(DWORD), NULL);
}

void WriteMemString(PMASK *pmask, int target, char *buf, int len)
{
	WriteProcessMemory(pmask->pground, (LPVOID)target, (LPCVOID)buf, (size_t)len, NULL);
}

int IsNoxProcessOn(PMASK *pmask)
{
	return GetMemory(pmask, TEST_CHECK_ADDR);
}

int NoxCurrentFrames(PMASK *pmask)
{
	return GetMemory(pmask, 0x84ea04);
}

int IsInGameMode(PMASK *pmask)
{
	return GetMemory(pmask, 0x6d8530);
}