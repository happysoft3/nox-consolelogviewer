
#include "head.h"
#include "nox_func.h"
#include "noxMem.h"


void NoxConsolePrint(PMASK *pmask, char *str, int color)
{
	BYTE codes[] = { 0x56, 0x68, 0x90, 0x0B, 0x45, 0x00, 0xBE, 0x50, 0xFE, 0x69, 0x00, 0xB8, 0xF0, 0x3A, 0x75,
		0x00, 0xFF, 0x30, 0xFF, 0x36 , 0xFF , 0x54 , 0x24 , 0x08 , 0x83 , 0xC4 , 0x0C , 0x5E , 0x31 , 0xC0 , 0xC3 , 0x90 };
	wchar_t w_str[400] = { 0, };
	void *str_arg = VirtualAllocEx(pmask->pground, NULL, sizeof(w_str), MEM_COMMIT, PAGE_READWRITE),
		*func_addr = VirtualAllocEx(pmask->pground, NULL, sizeof(codes), MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	HANDLE pExec;
	int *sArg = (int *)&codes[12];

	*sArg = CON_STR_ADDR;

	MultiByteToWideChar(CP_ACP, 0, str, strlen(str), w_str, sizeof(w_str));

	WriteMemString(pmask, (int)str_arg, (char *)w_str, sizeof(w_str));
	SetMemory(pmask, CON_STR_ADDR, (int)str_arg);
	SetMemory(pmask, CON_COLOR, color);

	WriteMemString(pmask, (int)func_addr, codes, sizeof(codes));

	pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)func_addr, NULL, 0, NULL);
	WaitForSingleObject(pExec, 2000);

	VirtualFreeEx(pmask->pground, str_arg, 0, MEM_RELEASE);
	VirtualFreeEx(pmask->pground, func_addr, 0, MEM_RELEASE);
}

unsigned WINAPI ThreadNoxCmdLine(void *thread_ptr)
{
	__asm
	{
		push 0x443c80
		push 1
		mov eax, CON_STR_ADDR
		mov eax, [eax]
		push eax
		call[esp + 8]
		add esp, 0xc
		xor eax, eax
	};
	return 0;
}

void NoxCmdLine(PMASK *pmask, wchar_t *cmd)
{
	DWORD func_len = (DWORD)NoxCmdLine - (DWORD)ThreadNoxCmdLine;
	NX_CMD nox_con;
	void *arg_addr = VirtualAllocEx(pmask->pground, NULL, sizeof(nox_con), MEM_COMMIT, PAGE_READWRITE),
		*func_addr = VirtualAllocEx(pmask->pground, NULL, (size_t)func_len, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	HANDLE pExec;
	
	nox_con.nox_func_addr = ThreadNoxCmdLine;
	lstrcpyW(nox_con.msg, cmd);
	//strcpy_s(create_unit.unit_name, sizeof(char) * 100, name);
	WriteProcessMemory(pmask->pground, (LPVOID)arg_addr, (LPCVOID)&nox_con, sizeof(nox_con), NULL);
	WriteProcessMemory(pmask->pground, (LPVOID)CON_STR_ADDR, (LPCVOID)&arg_addr, sizeof(DWORD), NULL);
	WriteProcessMemory(pmask->pground, (LPVOID)func_addr, (LPCVOID)nox_con.nox_func_addr, (size_t)func_len, NULL);

	pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)func_addr, NULL, CREATE_SUSPENDED, NULL);
	ResumeThread(pExec);
	WaitForSingleObject(pExec, 2000);

	VirtualFreeEx(pmask->pground, arg_addr, 0, MEM_RELEASE);
	VirtualFreeEx(pmask->pground, func_addr, 0, MEM_RELEASE);
}

unsigned WINAPI ThreadCreateCoopTeam(void *thread_ptr)
{
	__asm
	{
		push 0x417E10
		call[esp]
			add esp, 0x4
	};
	return 0;
}

void CreateCoopTeam(PMASK *pmask, wchar_t *cmd)
{
	DWORD func_len = (DWORD)CreateCoopTeam - (DWORD)ThreadCreateCoopTeam;
	NX_CMD nox_con;
	void *arg_addr = VirtualAllocEx(pmask->pground, NULL, sizeof(nox_con), MEM_COMMIT, PAGE_READWRITE),
		*func_addr = VirtualAllocEx(pmask->pground, NULL, (size_t)func_len, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	HANDLE pExec;

	nox_con.nox_func_addr = ThreadCreateCoopTeam;
	lstrcpyW(nox_con.msg, cmd);
	//strcpy_s(create_unit.unit_name, sizeof(char) * 100, name);
	WriteProcessMemory(pmask->pground, (LPVOID)arg_addr, (LPCVOID)&nox_con, sizeof(nox_con), NULL);
	WriteProcessMemory(pmask->pground, (LPVOID)CON_STR_ADDR, (LPCVOID)&arg_addr, sizeof(DWORD), NULL);
	WriteProcessMemory(pmask->pground, (LPVOID)func_addr, (LPCVOID)nox_con.nox_func_addr, (size_t)func_len, NULL);

	pExec = CreateRemoteThread(pmask->pground, NULL, 0, (LPTHREAD_START_ROUTINE)func_addr, NULL, CREATE_SUSPENDED, NULL);
	ResumeThread(pExec);
	WaitForSingleObject(pExec, 2000);

	VirtualFreeEx(pmask->pground, arg_addr, 0, MEM_RELEASE);
	VirtualFreeEx(pmask->pground, func_addr, 0, MEM_RELEASE);
}
