#include "listViewHandler.h"
#include "noxMem.h"
#include "general.h"

#include "chatWnd.h"

LRESULT OnCustomDrawListView(HWND hList, LPARAM lParam)
{
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)lParam;

	switch (lplvcd->nmcd.dwDrawStage) {
	case CDDS_PREPAINT:
		return CDRF_NOTIFYITEMDRAW;
	case CDDS_ITEMPREPAINT:
		if (ListView_GetItemState(hList, lplvcd->nmcd.dwItemSpec, LVIS_FOCUSED | LVIS_SELECTED) == (LVIS_FOCUSED | LVIS_SELECTED))
		{
			int colWidth = 0;
			RECT ItemRect;
			RECT textRect;
			wchar_t iSubTxt[0x200];
			HRGN bgRgn;
			HBRUSH MyBrush;

			ListView_GetItemRect(hList, lplvcd->nmcd.dwItemSpec, &ItemRect, LVIR_BOUNDS);
			ItemRect.left += 5;
			bgRgn = CreateRectRgnIndirect(&ItemRect);
			MyBrush = CreateSolidBrush(RGB(PickMyData(38), PickMyData(39), PickMyData(40)));
			FillRgn(lplvcd->nmcd.hdc, bgRgn, MyBrush);
			DeleteObject(MyBrush);
			DeleteObject(bgRgn);

			SetTextColor(lplvcd->nmcd.hdc, RGB(PickMyData(41), PickMyData(42), PickMyData(43)));

			for (int i = 0; i < 4; i++)
			{
				if (i) colWidth += ListView_GetColumnWidth(hList, i - 1);
				ListView_GetItemText(hList, lplvcd->nmcd.dwItemSpec, i, iSubTxt, 0x200);
				SetRect(&textRect, ItemRect.left + colWidth, ItemRect.top, ItemRect.left + colWidth + ListView_GetColumnWidth(hList, i) - 5, ItemRect.bottom);
				//SetRect(&textRect, ItemRect.left + colWidth, ItemRect.top, ItemRect.right, ItemRect.bottom);
				DrawText(lplvcd->nmcd.hdc, iSubTxt, -1, &textRect, DT_LEFT); // | DT_NOCLIP);
			}
			return CDRF_SKIPDEFAULT;
		}
	}
	return CDRF_NOTIFYSUBITEMDRAW;
}

void OnDispInfoListView(LPARAM lParam, ConsoleMsg **cMsg)
{
	MyMainProc *mProc = PROC_MASK;
	LPNMHDR hdr = (LPNMHDR)lParam;
	NMLVDISPINFO *ndi = (NMLVDISPINFO *)lParam;
	char *picStr = NULL, mapSizeWchar[0x20];
	wchar_t szTmp[0x200] = { 0, };
	int cIndex = ndi->item.iItem, time;

	switch (ndi->item.iSubItem)
	{
	case 0:
		sprintf_s(mapSizeWchar, sizeof(mapSizeWchar), PickMyStringFromIndex(82), cIndex);
		picStr = mapSizeWchar;
		break;
	case 1:
		lstrcpy(szTmp, (wchar_t *)cMsg[cIndex]->content);
		picStr = NULL;
		break;
	case 2:
		time = cMsg[cIndex]->time;
		sprintf_s(mapSizeWchar, sizeof(mapSizeWchar), PickMyStringFromIndex(80), (time >> 16) & 0xff, (time >> 8) & 0xff, time & 0xff);
		picStr = mapSizeWchar;
		break;
	case 3:
		sprintf_s(mapSizeWchar, sizeof(mapSizeWchar), PickMyStringFromIndex(81), cMsg[cIndex]->color);
		picStr = mapSizeWchar;
	}
	if (picStr != NULL)
		MyWideCharConvert(picStr, szTmp, strlen(picStr) + 1);
	lstrcpy(ndi->item.pszText, szTmp);
}

void DisplaySelectColumnData(MyMainProc *mProc, int cur)
{
	mProc->listSel = cur;
}

void OnStateChangeListView(void *ptr, LPARAM lParam)
{
	NMLISTVIEW *pnmv = (LPNMLISTVIEW)lParam;
	MyMainProc *mProc = ptr;

	if ((pnmv->uNewState ^ pnmv->uOldState) & LVIS_SELECTED)
	{
		DisplaySelectColumnData(mProc, pnmv->iItem);
	}
}

int IsNumberText(short pic)
{
	return (pic >= 0x30 && pic <= 0x39);
}

int MatchNumbering(wchar_t *wKey)
{
	int wLen = lstrlen(wKey);

	if (IsNumberText(wKey[wLen - 1]))
		return _wtoi(wKey);
	else
		return -1;
}

int LogListViewKeyFind(MyMainProc *mProc, LPARAM lParam)
{
	int pResult = -1, itemCount = mProc->conLineCount;
	LPNMLVFINDITEM pnmfi = (LPNMLVFINDITEM)lParam;
	wchar_t *search = pnmfi->lvfi.psz;
	int fLine, mxLen = (sizeof(short) * 6);

	if (lstrlen(search) < mxLen && search[0] == 0x30)
	{
		fLine = MatchNumbering(search + 1);
		if (fLine < itemCount && fLine + 1)
		{
			wchar_t lineTo[100] = { 0, };

			pResult = fLine;
			wsprintf(lineTo, PickMyWideStringFromIndex(111), fLine);
			SetWindowText(mProc->ui_ptr->hEditOut, lineTo);
		}
	}
	return pResult;
}

void MapListViewOnDoubleClick(HWND hDlg, LPARAM lParam, ConsoleDat *conData)
{
	ConsoleMsg **cMsg = conData->cMsg;
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE)lParam;
	int rowItem = lpnmitem->iItem, subItm;
	
	if (rowItem >= 0 && conData->conMsgCount)
	{
		if (rowItem + 1 <= conData->conMsgCount)
		{
			subItm = lpnmitem->iSubItem;
			if (subItm == 1)
				UpdateChatWnd(hDlg, cMsg[rowItem]->content, cMsg[rowItem]->time);
		}
	}
}

LRESULT MapListViewOnDisplay(HWND hWnd, MyMainProc *mProc, LPARAM lParam)
{
	HWND hList = mProc->ui_ptr->conList;
	LPNMHDR hdr = (LPNMHDR)lParam;

	if (hdr->hwndFrom == hList)
	{
		switch (hdr->code)
		{
		case LVN_GETDISPINFO:
			OnDispInfoListView(lParam, mProc->conDataPtr->cMsg);
			break;
		case LVN_ODCACHEHINT:
		{
			LPNMLVCACHEHINT lpCacheHint = (LPNMLVCACHEHINT)lParam;
		}
		return 0;
		case NM_CUSTOMDRAW:
			return OnCustomDrawListView(hList, lParam);
		case LVN_ITEMCHANGED:
			OnStateChangeListView(mProc, lParam);
			break;
		case NM_DBLCLK:
			MapListViewOnDoubleClick((HWND)mProc->chatViewDlg, lParam, mProc->conDataPtr);
			break;
		case LVN_ODFINDITEM:
			return LogListViewKeyFind(mProc, lParam);
		}
	}
	return 0;
}

void NextMapFocusOnList(HWND hList, int *cursel, int ItemCounts, int prevCount)
{
	int pick;

	if (ItemCounts && *cursel >= 0)
	{
		if (*cursel == prevCount - 1)
		{
			pick = ItemCounts - 1;
			ListView_SetItemState(hList, pick, LVNI_SELECTED | LVNI_FOCUSED, LVNI_SELECTED | LVNI_FOCUSED);
			ListView_EnsureVisible(hList, pick, 0);
			SetFocus(hList);
		}
	}
}

void UpdateConsoleLogList(HWND hList, int updateCount, int *curSel)
{
	int prevCount = ListView_GetItemCount(hList);

	//SendMessage(hList, WM_SETREDRAW, 0, 0);
	ListView_SetItemCount(hList, updateCount);
	if (*curSel)
		NextMapFocusOnList(hList, curSel, updateCount, prevCount);
	//SendMessage(hList, WM_SETREDRAW, 1, 0);
}